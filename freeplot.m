function varargout=freeplot(numlines)
% 可以手动画出平滑的曲线的函数，但是最多能画7条曲线

%检查输入参数
if nargin~=1
    numlines=5;%默认画5条曲线
end
if ~isreal(numlines) && length(numlines)~=1 && ...
        ndims(numlines)~=2 && numlines<=0
    error('输入参数必须是一个正整数！');
end
if numlines>7
    error('最多可以画7条曲线！');
    return;
end

%打开一个figure窗口
hFig=figure('Name','FreePlot---作者：PenG','NumberTitle','off');
%设置需要的应用程序数据
setappdata(gcf,'total_lines',numlines);%设置总的线条数
setappdata(gcf,'current_lines',1);%设置当前正在绘制的线是第几条
lines=cell(1,numlines);
setappdata(gcf,'lines',lines);%保存每条曲线所画的点
spline_lines=cell(1,numlines);
setappdata(gcf,'spline_lines',spline_lines);%保存每条曲线平滑后的点
colors={'b','g','r','c','m','y','k'};
setappdata(gcf,'colors',colors(1,1:numlines));%设置每条曲线的颜色
line_styles={'-',':','-.','--','-',':','-.'};
setappdata(gcf,'line_styles',line_styles(1,1:numlines));%设置每条曲线的线形
num_points=zeros(1,numlines);
setappdata(gcf,'num_points',num_points);%设置每条曲线的所画的点数
setappdata(gcf,'num_enters',0);%设置点击Enter键的次数
setappdata(gcf,'grid',1);%设置网格为显示
setappdata(gcf,'box',0);%设置盒子为关闭
setappdata(gcf,'xlabel',[]);%设置X轴注释为空
setappdata(gcf,'ylabel',[]);%设置Y轴注释为空
setappdata(gcf,'title',[]);%设置图形标题为空
setappdata(gcf,'axis_lim',[0 10 0 10]);%设置X轴和Y轴的范围都为0到10
setappdata(gcf,'xtick',[]);%设置X轴标签
setappdata(gcf,'ytick',[]);%设置Y轴标签
setappdata(gcf,'istick',0);%标志是否设置标签
setappdata(gcf,'islegend',0);%标志是否设置legend
setappdata(gcf,'legend',[]);%曲线legend

%对figure窗口做其他的设置
axis_lim=getappdata(gcf,'axis_lim');
axis(axis_lim);
set(gcf,'WindowButtonMotionFcn',{@mouse_moveFcn,1});
set(gcf,'WindowButtonDownFcn',{@mouse_downFcn,1});
set(gcf,'KeyPressFcn',{@key_pressFcn});
set(gcf,'Pointer','crosshair');
grid on
set(gca,'Color','white');
hold on

%鼠标移动时都会执行的函数
function mouse_moveFcn(obj,evt,flag)
if flag==1
    pt=get(gca,'CurrentPoint');
    x=pt(1,1);
    y=pt(1,2);
    xlim=get(gca,'XLim');
    ylim=get(gca,'YLim');
    if x>= xlim(1) && x<=xlim(2) && y>=ylim(1) && y<=ylim(2)
        %如果鼠标在坐标轴内部
        title(['Current Point: (' num2str(x) ',' num2str(y) ')']);
    else
        %如果鼠标在坐标轴外部
        str={'按TAB键来获得帮助信息'};
        title(str);
    end
else
    %取消该函数的响应
end

function mouse_downFcn(obj,evt,flag)
if flag==1
    num_points=getappdata(gcf,'num_points');
    current_lines=getappdata(gcf,'current_lines');
    if num_points(1,current_lines)>=0
        num_points(1,current_lines)=num_points(1,current_lines)+1;
    else
        num_points(1,current_lines)=num_points(1,current_lines)+2;
    end
    setappdata(gcf,'num_points',num_points);
    pt=get(gca,'CurrentPoint');
    lines=getappdata(gcf,'lines');
    lines{1,current_lines}(:,num_points(1,current_lines))=[pt(1,1);pt(1,2)];
    setappdata(gcf,'lines',lines);
    plot(pt(1,1),pt(1,2),'ro');
    axis_lim=getappdata(gcf,'axis_lim');
    axis(axis_lim);
else
    %取消该函数的响应
end

function key_pressFcn(obj,evt)
key=get(gcf,'CurrentCharacter');
switch key
    case 13 %Enter 画图完毕，开始连线
        cla;
        num_enters=getappdata(gcf,'num_enters');
        num_enters=num_enters+1;
        setappdata(gcf,'num_enters',num_enters);
        current_lines=getappdata(gcf,'current_lines');
        num_points=getappdata(gcf,'num_points');
        spline_lines=getappdata(gcf,'spline_lines');
        lines=getappdata(gcf,'lines');
        colors=getappdata(gcf,'colors');
        line_styles=getappdata(gcf,'line_styles');
        for i=1:current_lines
            t=1:num_points(1,i);
            ts=1:0.1:num_points(1,i);
            spline_lines{1,i}=spline(t,lines{1,i},ts);
            plot(spline_lines{1,i}(1,:),spline_lines{1,i}(2,:),[colors{1,i} line_styles{1,i}]);
        end
        setappdata(gcf,'spline_lines',spline_lines);
        plot(spline_lines{1,current_lines}(1,:),spline_lines{1,current_lines}(2,:),[colors{1,current_lines} line_styles{1,current_lines}]);
        save('spline_lines.mat', 'spline_lines');
        axis_lim=getappdata(gcf,'axis_lim');
        axis(axis_lim);
        tle=getappdata(gcf,'title');
        xble=getappdata(gcf,'xlabel');
        yble=getappdata(gcf,'ylabel');
        istick=getappdata(gcf,'istick');
        if istick==1
            xtick=getappdata(gcf,'xtick');
            ytick=getappdata(gcf,'ytick');
            set(gca,'XTick',xtick);
            set(gca,'YTick',ytick);
        end
        islegend=getappdata(gcf,'islegend');
        if islegend==1
            leg=getappdata(gcf,'legend');
            legend(leg);
        end
        title(tle);
        xlabel(xble);
        ylabel(yble);
        set(gcf,'WindowButtonMotionFcn',{@mouse_moveFcn,0});
        set(gcf,'WindowButtonDownFcn',{@mouse_downFcn,0});
        set(gcf,'Pointer','arrow');
        hold off
    case 32 %Space 删除上一个点
        current_lines=getappdata(gcf,'current_lines');
        num_points=getappdata(gcf,'num_points');
        spline_lines=getappdata(gcf,'spline_lines');
        colors=getappdata(gcf,'colors');
        line_styles=getappdata(gcf,'line_styles');
        lines=getappdata(gcf,'lines');
        lines{1,current_lines}=lines{1,current_lines}(:,1:end-1);
        num_points(1,current_lines)=num_points(1,current_lines)-1;
        setappdata(gcf,'num_points',num_points);
        setappdata(gcf,'lines',lines);
        if num_points(1,current_lines)<0
            msgbox('已经删除了所有的点了!','错误','modal');
            return;
        end
        cla;
        if current_lines==1
            plot(lines{1,1}(1,:),lines{1,1}(2,:),'ro');
        else
            for i=1:current_lines-1
                plot(spline_lines{1,i}(1,:),spline_lines{1,i}(2,:),[colors{1,i} line_styles{1,i}]);
            end
            plot(lines{1,current_lines}(1,:),lines{1,current_lines}(2,:),'ro');
        end
        axis_lim=getappdata(gcf,'axis_lim');
        axis(axis_lim);
    case 49 %number 1  设置标题，X轴注释，Y轴注释
        prompt={'标题：','X轴注释：','Y轴注释：'};
        name='设置标题';
        num_lines=1;
        defAns={'','',''};
        answ=inputdlg(prompt,name,num_lines,defAns);
        if isempty(answ)
            return;
        else
            setappdata(gcf,'title',answ{1});
            setappdata(gcf,'xlabel',answ{2});
            setappdata(gcf,'ylabel',answ{3});
        end
        title(answ{1});
        xlabel(answ{2});
        ylabel(answ{3});
    case 50 %number 2  添加另一条曲线
        current_lines=getappdata(gcf,'current_lines');
        current_lines=current_lines+1;
        setappdata(gcf,'current_lines',current_lines);
        set(gcf,'WindowButtonMotionFcn',{@mouse_moveFcn,1});
        set(gcf,'WindowButtonDownFcn',{@mouse_downFcn,1});
        set(gcf,'Pointer','crosshair');
        hold on;
        msgbox(['你可以画第' num2str(current_lines) '条曲线了.'],'提示','modal');
    case 51 %number 3  设置坐标范围
        prompt={'X轴坐标范围：','Y轴坐标范围：'};
        num_lines=1;
        name='设置坐标轴';
        defAns={'[0 10]','[0 10]'};
        answ=inputdlg(prompt,name,num_lines,defAns);
        if isempty(answ)
            return;
        else
            xlim=str2num(answ{1});
            ylim=str2num(answ{2});
            setappdata(gcf,'axis_lim',[xlim ylim]);
        end
        axis([xlim ylim]);
    case 52 %number 4  将图形保存为图片
                [filename, pathname] = uiputfile({'*.jpg','JPEG files';...
            '*.bmp','BMP files';...
            '*.fig','MATLAB Fig files'}, 'Pick an M-file');
        if isequal(filename,0) || isequal(pathname,0)
            return;
        else
            file=fullfile(pathname, filename);
        end
        saveas(gcf,file);
    case 53 %number 5  将曲线拷贝至剪切板
        hgexport(gcf,'-clipboard');
        msgbox('拷贝完毕，只要Ctrl+V粘贴就OK了。','提示','modal');
    case 54 %number 6  设置曲线颜色
        colors=getappdata(gcf,'colors');
        current_lines=getappdata(gcf,'current_lines');
        prompt=cell(1,current_lines);
        defAns=cell(1,current_lines);
        for i=1:current_lines
            prompt{1,i}=['曲线' num2str(i) ':'];
            defAns{1,i}=colors{1,i};
        end
        answ=inputdlg(prompt,'曲线颜色',1,defAns);
        if isempty(answ)
            return;
        end
        colors(1,1:current_lines)=answ(1,1:end);
        num_enters=getappdata(gcf,'num_enters');
        cla;
        spline_lines=getappdata(gcf,'spline_lines');
        line_styles=getappdata(gcf,'line_styles');
        lines=getappdata(gcf,'lines');
        for i=1:num_enters
            plot(spline_lines{1,i}(1,:),spline_lines{1,i}(2,:),[colors{1,i} line_styles{1,i}]);
        end
        plot(lines{1,current_lines}(1,:),lines{1,current_lines}(2,:),'ro');
        setappdata(gcf,'colors',colors);
    case 55 %number 7  设置曲线的线型
        colors=getappdata(gcf,'colors');
        line_styles=getappdata(gcf,'line_styles');
        current_lines=getappdata(gcf,'current_lines');
        prompt=cell(1,current_lines);
        defAns=cell(1,current_lines);
        for i=1:current_lines
            prompt{1,i}=['曲线' num2str(i) '的线型:'];
            defAns{1,i}=line_styles{1,i};
        end
        answ=inputdlg(prompt,'曲线线型',1,defAns);
        if isempty(answ)
            return;
        end
        line_styles(1,1:current_lines)=answ(1,1:end);
        num_enters=getappdata(gcf,'num_enters');
        cla;
        spline_lines=getappdata(gcf,'spline_lines');
        lines=getappdata(gcf,'lines');
        for i=1:num_enters
            plot(spline_lines{1,i}(1,:),spline_lines{1,i}(2,:),[colors{1,i} line_styles{1,i}]);
        end
        plot(lines{1,current_lines}(1,:),lines{1,current_lines}(2,:),'ro');
        setappdata(gcf,'line_styles',line_styles);
    case 56 %number 8  显示/隐藏网格
        gridd=getappdata(gcf,'grid');
        if gridd==1
            grid off
            setappdata(gcf,'grid',0);
        else
            grid on
            setappdata(gcf,'grid',1);
        end
    case 57 %number 9  显示/隐藏盒子
        boxx=getappdata(gcf,'box');
        if boxx==0
            box on
            setappdata(gcf,'box',1);
        else
            box off
            setappdata(gcf,'box',0);
        end
    case 48 %number 0  设置X轴和Y轴标签
        xtick=get(gca,'XTick');
        ytick=get(gca,'YTick');
        prompt={'X轴标签：','Y轴标签：'};
        defAns={num2str(xtick),num2str(ytick)};
        answ=inputdlg(prompt,'设置标签',1,defAns);
        if isempty(answ)
            return;
        end
        setappdata(gcf,'xtick',str2num(answ{1}));
        setappdata(gcf,'ytick',str2num(answ{2}));
        set(gca,'XTick',str2num(answ{1}));
        set(gca,'YTick',str2num(answ{2}));
        setappdata(gcf,'istick',1);
    case 97 % a  为曲线添加标签
        current_lines=getappdata(gcf,'current_lines');
        prompt=cell(1,current_lines);
        defAns=cell(1,current_lines);
        for i=1:current_lines
            prompt{1,i}=['曲线' num2str(i) '的标签：'];
            defAns{1,i}=['data' num2str(i)];
        end
        answ=inputdlg(prompt,'曲线标签',1,defAns);
        if isempty(answ)
            return;
        end
        setappdata(gcf,'islegend',1);
        setappdata(gcf,'legend',answ);
        legend(answ);
    case 115 % s  添加文字
        prompt={'要添加的文字：','字体大小：'};
        defAns={'PenG','12'};
        answ=inputdlg(prompt,'添加文字',1,defAns);
        if isempty(answ)
            return;
        end
        gtext(answ{1},'FontSize',str2num(answ{2}));
    case 27 %ESC  查看作者信息
        str={'作者: 彭军','博客: http://hi.baidu.com/pengjun',...
            '邮件: hellotim@foxmail.com','QQ: 290596664'};
        msgbox(str,'作者信息','modal');
    case 9 %TAB  显示帮助信息
        str={'Enter键：画点完毕，进行平滑',...
            '空格键：删除上一个点',...
            '数字1键：设置标题，X轴注释和Y轴注释',...
            '数字2键：添加一条曲线',...
            '数字3键：设置XY轴坐标范围',...
            '数字4键：另存为图片',...
            '数字5键：拷贝图片至剪切板',...
            '数字6键：设置曲线的颜色',...
            '数字7键：设置曲线的线型',...
            '数字8键：显示/隐藏网格',...
            '数字9键：显示/隐藏盒子',...
            '数字0键：设置X轴和Y轴标签',...
            '字母a键：为曲线添加标签',...
            '字母s键：添加文字',...
            'TAB键：查看帮助',...
            'Esc键：查看作者信息',...
            '其他键：访问作者博客'};
        msgbox(str,'帮助','modal');
    otherwise %访问作者博客
        web http://hi.baidu.com/pengjun -browser
end