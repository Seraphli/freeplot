这个程序可以通过指定几个点来生成曲线，其原作者为[PenG](http://blog.csdn.net/pengqianhe/article/details/8107359)。
本人在其基础上修改，能够**输出曲线的点**，点**保存在spline_lines.mat**中。
本人通过邮件询问作者是否可以将其代码上传开源，但是作者在几个月中都没有回应。
本着开源的精神，本人将代码上传开源，希望能够给有需要的人提供帮助。

This program is used to generate a curve from given points.
The original author is [PenG](http://blog.csdn.net/pengqianhe/article/details/8107359).
I add some code in order to **save generated curve data to a file named spline_lines.mat**.
I try to contact the original author by e-mail, but there is no reply for several months.
I upload this code in order to help those who may find this code useful.
Hope this can save you some time.
